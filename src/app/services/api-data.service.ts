import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Data} from '../module/data';

@Injectable({
  providedIn: 'root'
})
export class ApiDataService {

  constructor(private http: HttpClient) { }
  /*trae datos*/
  getData(url: string) {
    return this.http.get<Data>(environment.apiUrl + url );
  }
}
