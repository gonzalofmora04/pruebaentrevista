import {EventEmitter, Injectable} from '@angular/core';

export interface Alert{
  texto: string;
  clase: string;
  show: boolean;
}

/*servicio de emisicion de mensaje para alerta*/
@Injectable({
  providedIn: 'root'
})

export class MessageService {
  alerta: EventEmitter<object> = new EventEmitter<object>();

  constructor() {
  }
  emitChangeAlert(objeto: Alert) {
    this.alerta.emit(objeto);
  }
}
