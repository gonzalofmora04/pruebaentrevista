export interface Data {
  data?: any ;
  error?: any ;
  success: boolean ;
}

export interface Ejercicio1 {
  data?: any;
  orden: any;
  tabla: any;
}

export interface FilaEjercicio1 {
  num: number;
  cont: number;
  inx: number;
  inI: number;
  inF: number;
}


