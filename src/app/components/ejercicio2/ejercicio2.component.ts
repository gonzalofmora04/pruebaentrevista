import {Component, OnInit} from '@angular/core';
import {ApiDataService} from '../../services/api-data.service';
import {Alert, MessageService} from '../../services/message.service';

@Component({
  selector: 'app-ejercicio2',
  templateUrl: './ejercicio2.component.html',
  styleUrls: ['./ejercicio2.component.css']
})
export class Ejercicio2Component {
  constructor(
    private data: ApiDataService,
    private message: MessageService
  ) {
  }

  /*******************/
  private regxNumber = new RegExp(/((\d+)(\.|\,)\d+)|(\d+)/, 'g');
  private alfabeto = 'abcdefghijklmnñopqrstuvwxyz';
  public tablaEjercicio2 = [];
  /*variable btn loading*/
  public loading = false;
  /*variable alerta emitir*/
  private alerta: Alert = {
    texto: '',
    show: true,
    clase: 'success'
  };

  /*******************/

  /*busca data */
  getData(): void {
    this.loading = true;
    this.data.getData('dict.php').subscribe(data => {

      if (data.success) {
        this.tablaEjercicio2 = this.ejercicio2Main(JSON.parse(data.data));
        this.alerta.clase = 'success';
        this.alerta.texto = 'Exitoso';
      } else {
        this.alerta.clase = 'danger';
        this.alerta.texto = data.error ? data.error : 'Error';
      }
      this.loading = false;
      this.message.emitChangeAlert(this.alerta);
    }, this.errorSubscription);
  }

  /*error de subcripcion*/
  errorSubscription() {
    this.alerta.clase = 'danger';
    this.alerta.texto = 'Error';
    this.message.emitChangeAlert(this.alerta);
  }

  /*recorre data genera objeto de tabla con datos solicitados*/
  ejercicio2Main(array?: any) {
    return array.map(x => {
      let {paragraph = ''} = x;
      return {
        letras: this.contarCaracteres(paragraph),
        numeros: this.sumarYencontrarNumeros(paragraph)
      };
    });
  }

  /*funcion encargada de buscar numeros en cadena y sumarlos*/
  sumarYencontrarNumeros(cadena: string) {
    /* busca numeros es cadena con expresion regular*/
    const exist = cadena.match(this.regxNumber);
    let numeros = 0;
    try {
      /*si existen numeros se recorre arreglo y suma numeros*/
      if (exist) {
        numeros = [...exist].map(x => {
          x = x.replace(/\./g, '');
          return +x;
        }).reduce((x, y) => x + y);
      }
    } catch (e) {
      console.log(e, 'Error');
    }
    return numeros;
  }

  /*recorre string alfabeto compara con expresion regular y suma cantidad de veces en que aparece*/

  contarCaracteres(cadena: string) {
    const caracter = [...this.alfabeto].map(x => {
      let regx = new RegExp(x, 'ig');
      return {
        letra: x
        , total: cadena.match(regx) ? cadena.match(regx).length : 0
      };
    });
    return caracter;
  }
}
