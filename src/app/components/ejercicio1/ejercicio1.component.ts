import {Component} from '@angular/core';
import {ApiDataService} from '../../services/api-data.service';
import {Ejercicio1} from '../../module/data';
import {Alert, MessageService} from '../../services/message.service';

@Component({
  selector: 'app-ejercicio1',
  templateUrl: './ejercicio1.component.html',
  styleUrls: ['./ejercicio1.component.css']
})
export class Ejercicio1Component {
  constructor(
    private data: ApiDataService,
    private message: MessageService
  ) {}


  public tablaEjercicio1: Ejercicio1 = {
    tabla: [],
    orden: [],
    data: []
  };

  /*variable btn loading*/
  public loading = false;
  /*variable alerta emitir*/
  private alerta: Alert = {
    texto: '',
    show: true,
    clase: 'success'
  };

  /*busca data y genera objeto para generar tabla*/
  getData(): void {
    /*Activa btn cargando*/
    this.loading = true;
    this.data.getData('array.php').subscribe((data) => {
      /*si respuesta es success=true se ejecuta si no ejecuta error message alerta*/
      if (data.success) {
        this.ejercicio1Main(data.data);
        this.alerta.clase = 'success';
        this.alerta.texto = 'Exitoso';
      } else {
        this.alerta.clase = 'danget';
        this.alerta.texto = data.error ? data.error : 'Error';
      }
      this.loading = false;
      this.message.emitChangeAlert(this.alerta);
    }, this.errorSubscription);
  }

  /*error de subcripcion*/
  errorSubscription() {
    this.alerta.clase = 'danger';
    this.alerta.texto = 'Error';
    this.message.emitChangeAlert(this.alerta);
  }


  /*metodo que se ejecuta si resultado es ok*/
  ejercicio1Main(array) {
    let filasTable = {};
    /*recorro array original buscando concurrencias*/
    for (const i of array) {
      filasTable [i] = this.contarOcurrencias(array, i);
    }
    /*objeto iterable para tabla*/
    this.tablaEjercicio1 = Object.assign({},
      this.tablaEjercicio1, {
        data: array,
        orden: this.ordenarData(array),
        tabla: Object.values(filasTable)
      });
  }

  /*cuanta cuantas veces se encuentra el numero en el array*/
  contarOcurrencias(data, num: number): object {
    let contador = 0;
    let index = data.indexOf(num);
    let indices = [];
    /*recorre array buscando numero e indice */
    while (index !== -1) {
      contador++;
      indices.push(index);
      index = data.indexOf(num, index + 1);
    }
    return Object.assign({},
      {
        numero: num,
        cont: contador,
        inx: indices,
        inI: indices[0],
        inF: indices[indices.length - 1]
      });

  }

  /*ordena data remplzando un numero pivote arreglo quicksort*/
  ordenarData(data: any): any {
    if (data < 1) {
      return [];
    }
    let left = [];
    let right = [];
    let pivot = data[0];
    for (let i = 1; i < data.length; i++) {
      if (data[i] < pivot) {
        left.push(data[i]);
      } else {
        right.push(data[i]);
      }
    }
    return [].concat(this.ordenarData(left), pivot, this.ordenarData(right));
  }

}
