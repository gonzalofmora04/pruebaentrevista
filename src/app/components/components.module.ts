import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Ejercicio1Component} from './ejercicio1/ejercicio1.component';
import {Ejercicio2Component} from './ejercicio2/ejercicio2.component';
import {NavbarComponent} from './navbar/navbar.component';
import { BtnloadingComponent } from './btnloading/btnloading.component';
import { AlertaComponent } from './alerta/alerta.component';



@NgModule({
  declarations: [
    Ejercicio1Component,
    Ejercicio2Component,
    NavbarComponent,
    BtnloadingComponent,
    AlertaComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    Ejercicio1Component,
    Ejercicio2Component,
    NavbarComponent,
    AlertaComponent
  ]
})
export class ComponentsModule { }
