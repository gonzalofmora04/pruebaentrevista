import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-btnloading',
  templateUrl: './btnloading.component.html',
  styleUrls: ['./btnloading.component.css']
})
export class BtnloadingComponent  {
  @Input() texto: string;
  @Input() loading: boolean;
  constructor() { }



}
