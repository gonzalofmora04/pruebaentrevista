import { Component, OnInit } from '@angular/core';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-alerta',
  templateUrl: './alerta.component.html',
  styleUrls: ['./alerta.component.css']
})
/*Componente alerta*/
export class AlertaComponent implements OnInit {
  public texto = '';
  public show = false;
  public clase = 'alert-primary';
  constructor(private message: MessageService) { }

  ngOnInit() {
    this.message.alerta.subscribe(data => {
      this.texto = data.texto;
      this.show = data.show;
      this.clase = `alert-${data.clase}`;
      /*quita alerta despues de 1sg*/
       setTimeout(() => {
        this.show = !this.show;
      }, 1000);
    });
  }

}
